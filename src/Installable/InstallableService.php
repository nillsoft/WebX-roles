<?php

namespace WebX\Installable;

interface InstallableService {

    public function install();
}