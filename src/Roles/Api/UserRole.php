<?php

namespace WebX\Roles\Api;

use WebX\Routes\Api\Map;

interface UserRole extends Role {

    /**
     * @return Account
     */
    public function account();


}