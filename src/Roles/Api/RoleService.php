<?php

namespace WebX\Roles\Api;

interface RoleService {

    /**
     * @return Account[]
     */
    public function listAccounts();

    /**
     * @return User[]
     */
    public function listUsers();


    /**
     * @param $email
     * @return User|null
     */
    public function findUserByEmail($email);

    /**
     * @param string $id
     * @return User|null
     */
    public function findUserById($id);

    /**
     * @param string $id
     * @return AccountRole|null
     */
    public function findAccountRoleById($id);

    /**
     * @param string $id
     * @return UserRole|null
     */
    public function findUserRoleById($id);


    /**
     * @param string $id
     * @return Account|null
     */
    public function findAccountById($id);

    /**
     * @return Account
     */
    public function createAccount();

    /**
     * @return User
     */
    public function createUser();


    /**
     * @param User $user
     * @param int $minutesToLive
     * @return string
     */
    public function createResetPasswordToken(User $user, $minutesToLive);

    /**
     * @param string $token
     * @return User|null
     */
    public function findUserByPasswordToken($token);

    /**
     * @param string $token
     * @return bool
     */
    public function deletePasswordToken($token);

}