<?php

namespace WebX\Roles\Api;

use WebX\Routes\Api\Map;
use WebX\Routes\Api\WritableMap;

interface Role {

    /**
     * @return string
     */
    public function id();

    /**
     * @return WritableMap
     */
    public function properties();

    /**
     * @return void
     */
    public function save();

    /**
     * @return void
     */
    public function delete();

    /**
     * @param User $user
     * @return bool
     */
    public function ownedByUser(User $user);

    /**
     * @param Account $account
     * @return bool
     */
    public function ownedByAccount(Account $account);

}