<?php

namespace WebX\Roles\Api;

use WebX\Routes\Api\Map;

interface User {

    /**
     * @return string
     */
    public function id();

    /**
     * @return string
     */
    public function email();


    /**
     * @return string
     */
    public function firstName();

    /**
     * @return string
     */
    public function lastName();

    /**
     * @param string $email
     * @return void
     */
    public function setEmail($email);

    /**
     * @param $name
     * @return void
     */
    public function setFirstName($firstName);

    /**
     * @param $name
     * @return void
     */
    public function setLastName($lastName);

    /**
     * @param $password
     * @return void
     */
    public function setPassword($password);

    /**
     * @param string $key dot notated string
     * @param mixed $value
     * @return void
     */
    public function setProperty($key,$value);

    /**
     * @return Map
     */
    public function properties();

    /**
     * @param string $id
     * @return mixed
     */
    public function property($id);

    /**
     * @return void
     */
    public function save();

    /**
     * @return UserRole[]
     */
    public function roles();

    /**
     * @param Account $account
     * @return UserRole
     */
    public function createRole(Account $account);

    /**
     * @param string $roleId
     * @return UserRole
     */
    public function findRoleById($roleId);


    /**
     * @param string $email
     * @return bool
     */
    public function validatePassword($password);

    /**
     * Deletes the user and all its roles.
     * @return void
     */
    public function delete();
}