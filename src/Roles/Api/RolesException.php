<?php

namespace WebX\Roles\Api;

use Exception;

class RolesException extends Exception {

    private $id;

    private $params;

    public function __construct($id, array $params = null, Exception $cause = null) {
        parent::__construct($id . ($params ? json_encode($params) : ""),0,$cause);
        $this->id = $id;
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function id() {
        return $this->id;
    }

    /**
     * @return array
     */
    public function params() {
        return $this->params;
    }


}