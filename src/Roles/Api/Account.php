<?php

namespace WebX\Roles\Api;

use WebX\Routes\Api\Map;

interface Account {

    /**
     * @return string
     */
    public function id();


    /**
     * @return string
     */
    public function name();

    /**
     * @param string $name
     * @return void
     */
    public function setName($name);

    /**
     * @param Role $user
     * @return AccountRole
     */
    public function createRole(User $user);

    /**
     * @param string $id
     * @return AccountRole|null
     */
    public function findRole($id);

    /**
     * @return AccountRole[]
     */
    public function roles();

    /**
     * @param string $key dot notated string
     * @param mixed $value
     * @return void
     */
    public function setProperty($key,$value);

    /**
     * Deletes property
     * @param string $key
     */
    public function deleteProperty($key);

    /**
     * @return Map
     */
    public function properties();

    /**
     * @return void
     */
    public function save();

    /**
     * @return void
     */
    public function delete();
}