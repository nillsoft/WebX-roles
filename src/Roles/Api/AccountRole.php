<?php

namespace WebX\Roles\Api;

use WebX\Routes\Api\Map;

interface AccountRole extends Role {

    /**
     * @return User
     */
    public function user();

}