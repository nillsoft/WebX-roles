<?php

namespace WebX\Roles\Api;

interface UserRoleSession {

    /**
     * Sets the UserRole of this session
     * @param UserRole $role
     * @return void
     */
    public function setRole(UserRole $role);

    /**
     * @return UserRole|null
     */
    public function role();

    /**
     * @return UserRole|null
     */
    public function loggedInRole();


    /**
     * @return void
     */
    public function unsetUser();

    /**
     * @return User
     */
   public function user();


    /**
     * Set the user of the this session
     * @param User $user
     * @return void
     */
    public function setUser(User $user);

}