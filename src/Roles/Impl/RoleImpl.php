<?php
namespace WebX\Roles\Impl;


use JsonSerializable;
use triagens\ArangoDb\Document;
use triagens\ArangoDb\DocumentHandler;
use triagens\ArangoDb\Exception;
use WebX\Roles\Api\Account;
use WebX\Roles\Api\Role;
use WebX\Roles\Api\RolesException;
use WebX\Roles\Api\User;
use WebX\Routes\Api\WritableMap;
use WebX\Routes\Utils\MapUtil;

abstract class RoleImpl implements Role, JsonSerializable {

    /**
     * @var RoleServiceImpl
     */
    protected static $roleService;

    /**
     * @var ArangoUtil
     */
    protected static $arangoUtil;

    /**
     * @var string
     */
    private $id;

    /**
     * @var WritableMap
     */
    protected $properties;

    /**
     * UserImpl constructor.
     * @param $id
     * @param User $user
     * @param Account $account
     * @param WritableMap $properties
     */
    public function __construct($id, WritableMap $properties = null) {
        $this->id = $id;
        $this->properties = $properties ?: MapUtil::writable();
    }


    public static function init(RoleServiceImpl $roleService, ArangoUtil $arangoUtil) {
        self::$roleService = $roleService;
        self::$arangoUtil = $arangoUtil;
    }

    public final function id() {
        return $this->id;
    }

    public final function properties() {
        return $this->properties;
    }

    public final function save() {
        $docHandler = new DocumentHandler(self::$arangoUtil->connection());
        $state = $this->properties->raw();
        $state["_from"] = "webx_users/{$this->userId()}";
        $state["_to"] = "webx_accounts/{$this->accountId()}";
        $doc = Document::createFromArray($state);
        try {
            if ($this->id) {
                $docHandler->updateById("webx_roles", $this->id, $doc);
            } else {
                $this->id = $docHandler->save("webx_roles", $doc);
            }
        } catch(Exception $e) {
            throw new RolesException("unknownError",null,$e);
        }
    }

    public final function delete() {
        if($this->id) {
            $docHandler = new DocumentHandler(self::$arangoUtil->connection());
            $docHandler->removeById("webx_roles",$this->id);
            $this->id = null;
        }
    }

    public final function jsonSerialize() {

        $state = [
            "id" => $this->id,
            "properties" => $this->properties->raw()
        ];
        $this->onJsonSerialize($state);
        return $state;
    }

    public function ownedByUser(User $user) {
        return $this->userId()===$user->id();
    }

    public function ownedByAccount(Account $account) {
        return $this->accountId()===$account->id();
    }

    protected abstract function onJsonSerialize(array &$state);

    /**
     * @return string
     */
    protected abstract function userId();

    /**
     * @return string
     */
    protected abstract function accountId();

}