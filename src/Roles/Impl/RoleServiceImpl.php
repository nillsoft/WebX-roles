<?php

namespace WebX\Roles\Impl;


use triagens\ArangoDb\Collection;
use triagens\ArangoDb\CollectionHandler;
use triagens\ArangoDb\Connection;
use triagens\ArangoDb\Document;
use triagens\ArangoDb\DocumentHandler;
use WebX\Installable\InstallableService;
use WebX\Roles\Api\Account;
use WebX\Roles\Api\AccountRole;
use WebX\Roles\Api\RoleService;
use WebX\Roles\Api\User;
use WebX\Routes\Api\WritableMap;
use WebX\Routes\Utils\MapUtil;

class RoleServiceImpl implements RoleService, InstallableService{

    /**
     * @var ArangoUtil
     */
    private $arangoUtil;

    /**
     * UserServiceImpl constructor.
     * @param Connection $arango
     */
    public function __construct(Connection $arango) {
        $this->arango = $arango;
        $this->arangoUtil = new ArangoUtil($arango);
        UserImpl::init($this,$this->arangoUtil);
        AccountImpl::init($this,$this->arangoUtil);
        RoleImpl::init($this,$this->arangoUtil);

        //User->Role->Account

    }

    public function listAccounts() {
        $cursor = $this->arangoUtil->createCursor("FOR account IN webx_accounts SORT account.name ASC RETURN account");
        $accounts = [];

        /** @var Document $doc */
        foreach($cursor->getAll() as $doc) {
            $accounts[] = new AccountImpl($doc->getKey(),MapUtil::writable($doc->getAll()));
        }
        return $accounts;
    }

    public function listUsers() {
        $cursor = $this->arangoUtil->createCursor("FOR user IN webx_users SORT user.lastName,user.firstName ASC RETURN user");
        $users = [];

        /** @var Document $doc */
        foreach($cursor->getAll() as $doc) {
            $users[] = new UserImpl($doc->getKey(),MapUtil::writable($doc->getAll()));
        }
        return $users;
    }


    /**
     * @param $email
     * @return User|null
     */
    public function findUserByEmail($email) {
        if($email && ($email = strtolower(trim($email)))) {
            $cursor = $this->arangoUtil->createCursor("FOR user IN webx_users FILTER user.email==@email RETURN user", ["email" => $email]);
            $docs = $cursor->getAll();
            if ($docs) {
                /** @var Document[] $docs */
                $user = $docs[0];
                return new UserImpl($user->getKey(), MapUtil::writable($user->getAll()));
            }
        }
        return null;
    }

    public function findUserById($id) {
        $cursor = $this->arangoUtil->createCursor("FOR user IN webx_users FILTER user._key==@id RETURN user",["id"=>$id]);
        $docs = $cursor->getAll();
        if($docs) {
            /** @var Document[] $docs */
            $user = $docs[0];
            return new UserImpl($user->getKey(),MapUtil::writable($user->getAll()));
        }
        return null;
    }

    public function createAccount() {
        return new AccountImpl(null);
    }

    public function createUser() {
        return new UserImpl(null);
    }

    public function findAccountRoleById($id) {
        if($id) {
            $aql = "
                LET role = DOCUMENT(CONCAT('webx_roles/',@roleKey))
                FILTER role
                RETURN {
                    'user' : DOCUMENT(role._from),
                    'role' : role,
                    'accountId' : SPLIT(role._to,'/')[1]
                }";

            $cursor = $this->arangoUtil->createCursor($aql, ["roleKey"=>$id]);
            /** @var Document[] $docs */
            if($docs = $cursor->getAll()) {
                $map = MapUtil::writable($docs[0]->getAll());
                if ($roleMap = $map->asWritableMap("role")) {
                    $roleId = $roleMap->asString("_key");
                    ArangoUtil::removeInternals($roleMap);
                    if (($userMap = $map->asWritableMap("user"))) {
                        $userId = $userMap->asString("_key");
                        ArangoUtil::removeInternals($userMap);
                        return new AccountRoleImpl($roleId, $roleMap,new UserImpl($userId,$userMap), $map->asString("accountId"));
                    }
                }

            }
        }
        return null;
    }

    public function findUserRoleById($id) {
        if($id) {
            $aql = "
                LET role = DOCUMENT(CONCAT('webx_roles/',@roleKey))
                FILTER role
                RETURN {
                    'userId' : SPLIT(role._from,'/')[1],
                    'role' : role,
                    'account' : DOCUMENT(role._to)
                }";

            $cursor = $this->arangoUtil->createCursor($aql, ["roleKey"=>$id]);
            /** @var Document[] $docs */
            if($docs = $cursor->getAll()) {
                $map = MapUtil::writable($docs[0]->getAll());
                if ($roleMap = $map->asWritableMap("role")) {
                    $roleId = $roleMap->asString("_key");
                    ArangoUtil::removeInternals($roleMap);
                    if (($accountMap = $map->asWritableMap("account"))) {
                        $accountId = $accountMap->asString("_key");
                        ArangoUtil::removeInternals($accountMap);
                        return new UserRoleImpl($roleId, $roleMap,new AccountImpl($accountId,$accountMap), $map->asString("userId"));
                    }
                }

            }
        }
        return null;
    }


    public function install() {
        $handler = new CollectionHandler($this->arangoUtil->connection());
        if(!$handler->has("webx_users")) {
            $collection = new Collection();
            $collection->setName("webx_users");
            $collection->setType(Collection::TYPE_DOCUMENT);
            $id = $handler->create($collection);
            $handler->createHashIndex($id, ["email"], true);
        }
        if(!$handler->has("webx_accounts")) {
            $collection = new Collection();
            $collection->setName("webx_accounts");
            $collection->setType(Collection::TYPE_DOCUMENT);
            $id = $handler->create($collection);
            $handler->createHashIndex($id, ["name"], true);
        }
        if(!$handler->has("webx_roles")) {
            $collection = new Collection();
            $collection->setName("webx_roles");
            $collection->setType(Collection::TYPE_EDGE);
            $id = $handler->create($collection);
        }

        if(!$handler->has("webx_tokens")) {
            $collection = new Collection();
            $collection->setName("webx_tokens");
            $collection->setType(Collection::TYPE_DOCUMENT);
            $id = $handler->create($collection);
            $handler->createHashIndex($id, ["token"], true);
            $handler->createHashIndex($id, ["user"], false);
        }
    }

    public function findAccountById($id) {
        $cursor = $this->arangoUtil->createCursor("FOR account IN webx_accounts FILTER account._key==@id RETURN account",["id"=>$id]);
        $docs = $cursor->getAll();
        if($docs) {
            /** @var Document[] $docs */
            $account = $docs[0];
            return new AccountImpl($account->getKey(),MapUtil::writable($account->getAll()));
        }
        return null;
    }

    public function createResetPasswordToken(User $user, $minutesToLive) {
        $token = $this->generateToken(20);
        $doc = [
            "removeTimestamp" => time() + 60 * $minutesToLive,
            "user" => $user->id(),
            "token" => $token
        ];
        $docHandler = new DocumentHandler($this->arangoUtil->connection());
        $docHandler->save("webx_tokens",Document::createFromArray($doc));
        return $token;
    }

    public function findUserByPasswordToken($token) {
        $this->deleteOldTokens();
        $cursor = $this->arangoUtil->createCursor("
            FOR tokenDoc IN webx_tokens
                FILTER tokenDoc.token==@token
                RETURN tokenDoc.user
        ",["token"=>$token]);

        /** @var Document[] $documents */
        if($documents = $cursor->getAll()) {
            $userId = $documents[0];
            return $this->findUserById($userId);
        }
        return null;
    }

    private function deleteOldTokens() {
        $time = time();
        $cursor = $this->arangoUtil->createCursor("
            FOR tokenDoc IN webx_tokens
                FILTER tokenDoc.removeTimestamp<@time
                    REMOVE tokenDoc IN webx_tokens
                    RETURN 1    
        ",["time"=>$time]);
        return $cursor->getCount();
    }

    public function deletePasswordToken($token) {
        $cursor = $this->arangoUtil->createCursor("
            FOR tokenDoc IN webx_tokens
                FILTER tokenDoc.token==@token
                    REMOVE tokenDoc IN webx_tokens
                    RETURN 1    
        ",["token"=>$token]);
        return $cursor->getCount() > 0;
    }

    private function generateToken($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}