<?php

namespace WebX\Roles\Impl;

use JsonSerializable;
use triagens\ArangoDb\Document;
use triagens\ArangoDb\DocumentHandler;
use triagens\ArangoDb\Exception;
use WebX\Roles\Api\Account;
use WebX\Roles\Api\AccountException;
use WebX\Roles\Api\Role;
use WebX\Roles\Api\RolesException;
use WebX\Roles\Api\User;
use WebX\Roles\Api\UserRole;
use WebX\Routes\Api\Map;
use WebX\Routes\Api\WritableMap;
use WebX\Routes\Utils\MapUtil;

class UserImpl implements User, JsonSerializable {

    /**
     * @var RoleServiceImpl
     */
    private static $roleService;

    /**
     * @var ArangoUtil
     */
    private static $arangoUtil;

    /**
     * @var string
     */
    private $id;

    /**
     * @var WritableMap
     */
    private $state;

    /**
     * @var Role[]
     */
    private $roles;

    /**
     * UserImpl constructor.
     * @param string $id
     * @param WritableMap $state
     */
    public function __construct($id, WritableMap $state = null) {
        $this->id = $id;
        $this->state = $state ?: MapUtil::writable();
    }

    public static function init(RoleServiceImpl $roleService, ArangoUtil $arangoUtil) {
        self::$roleService = $roleService;
        self::$arangoUtil = $arangoUtil;
    }

    public function createRole(Account $account) {
        return new UserRoleImpl(null,MapUtil::writable(),$account,$this->id());
    }

    public function roles() {
        if($this->id) {
            $aql = "FOR account,role IN 1..1 OUTBOUND CONCAT('webx_users/',@id) webx_roles 
                RETURN {
                    'role' : role,
                    'account' : account
                }";
            $cursor = self::$arangoUtil->createCursor($aql, ["id" => $this->id]);
            if($docs = $cursor->getAll()) {
                $roles = [];
                foreach($docs as $doc) {
                    $roles[] = $this->createRoleFromResultDocument($doc);
                }
                return $roles;
            }
        }
        return [];
    }

    public function findRoleById($roleId) {
        if ($this->id) {
            $cursor = self::$arangoUtil->createCursor("
            LET role = DOCUMENT(CONCAT('webx_roles/',@id))
            LET account = DOCUMENT(role._to)
            FILTER role._from == CONCAT('webx_users/',@userId)
            RETURN {
                'role' : role,
                'account' : account
            }
            ", ["id" => $roleId, "userId" => $this->id]);
            if ($docs = $cursor->getAll()) {
                /** @var Document[] $docs */
                return $this->createRoleFromResultDocument($docs[0]);
            } else {
                return null;
            }
        }
        return null;
    }

    private function createRoleFromResultDocument(Document $document) {
        $map = MapUtil::writable($document->getAll());
        if($roleMap = $map->asWritableMap("role")) {
            $roleId = $roleMap->asString("_key");
            ArangoUtil::removeInternals($roleMap);
            if(($accountMap = $map->asWritableMap("account"))) {
                $accountId = $accountMap->asString("_key");
                ArangoUtil::removeInternals($accountMap);
                return new UserRoleImpl($roleId,$roleMap, new AccountImpl($accountId,$accountMap),$this->id);
            }
        } else {
            return null;
        }
    }

    public function save() {
        if(!$this->email()) {
            throw new RolesException("emailIsMissing");
        }

        $docHandler = new DocumentHandler(self::$arangoUtil->connection());
        $doc = Document::createFromArray(ArangoUtil::removeInternals($this->state)->raw());
        try {
            

            if ($this->id) {
                $docHandler->updateById("webx_users", $this->id, $doc);
            } else {
                $this->id = $docHandler->save("webx_users", $doc);
            }
        } catch(Exception $e) {
            if($e->getCode()===409) {
                throw new RolesException("userEmailAlreadyExists",["email"=>$this->state->asString("email")]);
            } else {
                throw new RolesException("unknownError",null,$e);
            }
        }
    }

    public function setFirstName($firstName) {
        $this->state->set(trim(strval($firstName))?: null,"firstName");
    }

    public function setLastName($lastName) {
        $this->state->set(trim(strval($lastName))?: null,"lastName");
    }

    public function setPassword($password) {
        $this->state->set($this->hashString($password),"password");
    }

    public function firstName() {
        return $this->state->asString("firstName");
    }

    public function lastName() {
        return $this->state->asString("lastName");
    }

    public function setEmail($email) {
        if ($email = (trim(strval($email)) ?: null)) {
            $this->state->set(strtolower($email), "email");
        }
    }

    public function setProperty($key, $value) {
        if($value===null) {
            $this->state->delete("__{$key}");
        } else {
            $this->state->set($value,"__{$key}");
        }
    }

    private function getRawProperties() {
        $all = $this->state->raw();
        $properties = [];
        foreach($all as $key => $value) {
            if(strpos($key,"__")===0) {
                $properties[substr($key,2)] = $value;
            }
        }
        return $properties;
    }

    public function properties() {
        return MapUtil::readable($this->getRawProperties());
    }

    public function property($id) {
        $properties = $this->getRawProperties();
        return isset($properties[$id]) ? $properties[$id] : null;
    }


    private function hashString($string) {
        if($string = trim(strval($string))) {
            return md5($string);
        }
        return null;
    }

    /**
     * @return string
     */
    public function id() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function email() {
        return $this->state->asString("email");
    }

    public function validatePassword($password) {
        return $this->state->asString("password")===$this->hashString($password);
    }

    function jsonSerialize() {
        $data = [
            "id" => $this->id,
            "email" => $this->email(),
            "firstName" => $this->firstName(),
            "lastName" => $this->lastName()
        ];
        if($properties = $this->getRawProperties()) {
            $data["properties"] = $properties;
        }

        return $data;
    }

    public function delete() {
        if($this->id()) {
            $aql = "

            LET user = DOCUMENT(CONCAT('webx_users/',@userId))
            FILTER user
            //Remove roles
            LET removedRoles = (
                FOR account,role IN OUTBOUND user webx_roles
                    REMOVE role IN webx_roles
                    RETURN 1    
            )
            LET removedTokens = (
                FOR token IN webx_tokens
                    FILTER token.user == @userId
                    REMOVE token IN webx_tokens
                    RETURN 1
            )
            REMOVE user IN webx_users
            ";
            self::$arangoUtil->createCursor($aql,["userId"=>$this->id()]);
        }
    }


}