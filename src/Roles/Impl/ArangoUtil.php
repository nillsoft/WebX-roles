<?php

namespace WebX\Roles\Impl;

use triagens\ArangoDb\Connection;
use triagens\ArangoDb\Cursor;
use triagens\ArangoDb\Statement;
use WebX\Routes\Api\WritableMap;

class ArangoUtil {

    /**
     * @var Connection
     */
    private $arango;

    /**
     * ArangoUtil constructor.
     * @param string $owner
     * @param Connection $arango
     */
    public function __construct(Connection $connection) {
        $this->connection = $connection;
    }

    /**
     * @return Connection
     */
    public function connection() {
        return $this->connection;
    }

    /**
     * @param WritableMap $map
     * @return WritableMap Returns the same map
     */
    public static function removeInternals(WritableMap $map) {
        $map->delete("_key");
        $map->delete("_id");
        $map->delete("_rev");
        $map->delete("_to");
        $map->delete("_from");
        return $map;
    }


    /**
     * @param $aql
     * @param array $parameters
     * @return Cursor
     */
    public function createCursor($aql, array $parameters = [],$options = []) {

        $options = array_merge(
            [   "query"     => $aql,
                "count"     => true,
                "batchSize" => 1,
                "sanitize"  => true,
                "bindVars"  => $parameters,
            ],
            $options
        );

        $statement = new Statement(
            $this->connection,
            $options
        );
        return $statement->execute();
    }
}