<?php

namespace WebX\Roles\Impl;

use triagens\ArangoDb\Connection;
use WebX\Roles\Api\RoleService;
use WebX\Roles\Api\User;
use WebX\Roles\Api\UserRole;
use WebX\Roles\Api\UserRoleSession;
use WebX\Routes\Api\Routes;
use WebX\Routes\Api\WritableMap;

class UserRoleSessionImpl implements UserRoleSession {

    /**
     * @var User
     */
    private $user;

    /**
     * @var UserRole
     */
    private $role;

    /**
     * @var WritableMap
     */
    private $session;
    /**
     * UserServiceImpl constructor.
     * @param Connection $arango
     */
    public function __construct(Routes $routes, RoleService $roleService) {
        $session = $this->session = $routes->session("webxRole");
        if($userId = $session->asString("u")) {
            if($this->user = $roleService->findUserById($userId)) {
                if($roleId = $session->asString("r")) {
                    $this->role = $this->user->findRoleById($roleId);
                }
            }
        }
    }

    public function user() {
       return $this->user;
    }

    public function setUser(User $user) {
        $this->user = $user;
        $this->session->set($user->id(),"u");
    }

    public function setRole(UserRole $role) {
        $this->role = $role;
        $this->session->set($role->id(),"r");
    }

    public function role() {
        return $this->role;
    }

    public function loggedInRole() {
        if($this->user) {
         
            return $this->role;
        }
        return null;
    }


    public function unsetUser() {
        $this->session->delete("u");
        $this->role = null;
    }
}