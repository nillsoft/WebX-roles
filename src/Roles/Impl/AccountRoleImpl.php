<?php
namespace WebX\Roles\Impl;


use JsonSerializable;
use triagens\ArangoDb\Document;
use triagens\ArangoDb\DocumentHandler;
use triagens\ArangoDb\Exception;
use WebX\Roles\Api\Account;
use WebX\Roles\Api\AccountRole;
use WebX\Roles\Api\Role;
use WebX\Roles\Api\RolesException;
use WebX\Roles\Api\User;
use WebX\Roles\Api\UserRole;
use WebX\Routes\Api\WritableMap;
use WebX\Routes\Utils\MapUtil;

class AccountRoleImpl extends RoleImpl implements AccountRole {

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accountId;
    /**
     * @var
     */
    private $id;


    /**
     * AccountRoleImpl constructor.
     * @param string $id
     * @param WritableMap $properties
     * @param Account $account
     * @param string $userId
     */
    public function __construct($id, WritableMap $properties, User $user, $accountId) {
        parent::__construct($id,$properties);
        $this->user = $user;
        $this->accountId = $accountId;
    }

    protected function onJsonSerialize(array &$state) {
        $state["user"] = $this->user;
    }

    protected function userId() {
        return $this->user->id();
    }

    protected function accountId() {
        return $this->accountId;
    }

    public function account() {
   
        return $this->account();
    }

    public function user() {
        return $this->user;
    }
}