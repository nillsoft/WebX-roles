<?php

namespace WebX\Roles\Impl;

use JsonSerializable;
use triagens\ArangoDb\Document;
use triagens\ArangoDb\DocumentHandler;
use triagens\ArangoDb\Exception;
use WebX\Roles\Api\Account;
use WebX\Roles\Api\AccountRole;
use WebX\Roles\Api\Role;
use WebX\Roles\Api\RolesException;
use WebX\Roles\Api\User;
use WebX\Routes\Api\WritableMap;
use WebX\Routes\Utils\MapUtil;

class AccountImpl implements Account, JsonSerializable {

    /**
     * @var RoleServiceImpl
     */
    private static $roleService;

    /**
     * @var ArangoUtil
     */
    private static $arangoUtil;

    /**
     * @var string
     */
    private $id;

    /**
     * @var WritableMap
     */
    private $state;
    /**
     * UserImpl constructor.
     * @param string $id
     * @param string $email
     */
    public function __construct($id, WritableMap $state = null)
    {
        $this->id = $id;
        $this->state = $state ?: MapUtil::writable();
    }


    public static function init(RoleServiceImpl $roleService, ArangoUtil $arangoUtil) {
        self::$roleService = $roleService;
        self::$arangoUtil = $arangoUtil;
    }

    public function setName($name) {
        $this->state->set(trim(strval($name))?:null,"name");
    }

    public function save() {
        if(!$this->name()) {
            throw new RolesException("nameIsMissing");
        }
        $docHandler = new DocumentHandler(self::$arangoUtil->connection());
        $doc = Document::createFromArray($this->state->raw());
        try {
            if ($this->id) {
                $docHandler->updateById("webx_accounts", $this->id, $doc);
            } else {
                $this->id = $docHandler->save("webx_accounts", $doc);
            }
        } catch(Exception $e) {
            if($e->getCode()===409) {
                throw new RolesException("accountNameAlreadyExists",["name"=>$this->state->asString("name")]);
            } else {
                throw new RolesException("unknownError",null,$e);
            }
        }
    }
    public function setProperty($key, $value) {
        $this->state->set($value,"__{$key}");
    }

    public function deleteProperty($key) {
        $this->state->delete("__{$key}");
    }

    private function getRawProperties() {
        $all = $this->state->raw();
        $properties = [];
        foreach($all as $key => $value) {
            if(strpos($key,"__")===0) {
                $properties[substr($key,2)] = $value;
            }
        }
        return $properties;
    }

    public function properties() {
        return MapUtil::readable($this->getRawProperties());
    }

    public function createRole(User $user) {
        return new AccountRoleImpl(null,MapUtil::writable(),$user,$this->id);
    }

    public function findRole($id) {
        if($this->id) {
            $aql = "
                FOR user,role IN 1..1 INBOUND CONCAT('webx_accounts/',@accountId) webx_roles
                FILTER role._key == @roleId 
                RETURN {
                    'user' : user,
                    'role' : role
                }";

            $cursor = self::$arangoUtil->createCursor($aql, ["roleId"=>$id,"accountId"=>$this->id()]);
            /** @var Document[] $docs */
            if($docs = $cursor->getAll()) {
                $map = MapUtil::writable($docs[0]->getAll());
                if ($roleMap = $map->asWritableMap("role")) {
                    $roleId = $roleMap->asString("_key");
                    ArangoUtil::removeInternals($roleMap);
                    if (($userMap = $map->asWritableMap("user"))) {
                        $userId = $userMap->asString("_key");
                        ArangoUtil::removeInternals($userMap);
                        return new AccountRoleImpl($roleId, $roleMap,new UserImpl($userId,$userMap), $userId);
                    }
                }

            }
        }
        return null;

    }


    public function roles() {

        if($this->id) {
            $aql = "
                FOR user,role IN 1..1 INBOUND CONCAT('webx_accounts/',@id) webx_roles 
                RETURN {
                    'user' : user,
                    'role' : role
                }";

            $cursor = self::$arangoUtil->createCursor($aql, ["id" => $this->id]);
            if($docs = $cursor->getAll()) {
                $roles = [];
                foreach($docs as $doc) {
                    $map = MapUtil::writable($doc->getAll());
                    if ($roleMap = $map->asWritableMap("role")) {
                        $roleId = $roleMap->asString("_key");
                        ArangoUtil::removeInternals($roleMap);
                        if (($userMap = $map->asWritableMap("user"))) {
                            $userId = $userMap->asString("_key");
                            ArangoUtil::removeInternals($userMap);
                            $roles[] = new AccountRoleImpl($roleId, $roleMap,new UserImpl($userId,$userMap), $this->id);
                        }
                    }
                }
                return $roles;
            }
        }
        return [];

    }


    /**
     * @return string
     */
    public function id() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name() {
        return $this->state->asString("name");
    }

    function jsonSerialize() {
        return [
            "id" => $this->id,
            "name" => $this->name(),
            "properties" => $this->getRawProperties()
        ];
    }

    public function delete() {
        if($this->id()) {
            $aql = "

            LET account = DOCUMENT(CONCAT('webx_accounts/',@id))
            FILTER account

            LET accountUserRoles = (
               FOR user,role IN INBOUND account webx_roles
                    COLLECT u = user INTO roles = role
                    RETURN {
                        roles : roles,
                        user : u
                    }
            )
            LET roleDeletions = (FOR userRoles IN accountUserRoles
                FOR role IN userRoles.roles
                    REMOVE role IN webx_roles
                    RETURN 1
            )
            REMOVE account IN webx_accounts
            RETURN {
                roles: LENGTH(roleDeletions),
                account: 1
            }


            ";
            self::$arangoUtil->createCursor($aql,["id"=>$this->id()]);
        }
    }

}