<?php
namespace WebX\Roles\Impl;


use JsonSerializable;
use triagens\ArangoDb\Document;
use triagens\ArangoDb\DocumentHandler;
use triagens\ArangoDb\Exception;
use WebX\Roles\Api\Account;
use WebX\Roles\Api\Role;
use WebX\Roles\Api\RolesException;
use WebX\Roles\Api\User;
use WebX\Roles\Api\UserRole;
use WebX\Routes\Api\WritableMap;
use WebX\Routes\Utils\MapUtil;

class UserRoleImpl extends RoleImpl implements UserRole {

    /**
     * @var Account
     */
    private $account;

    /**
     * @var string
     */
    private $userId;

    /**
     * UserImpl constructor.
     * @param $id
     * @param WritableMap $properties
     * @param Account $account
     * @param string $userId
     */
    public function __construct($id, WritableMap $properties, Account $account,$userId) {
        parent::__construct($id,$properties);
        $this->account = $account;
        $this->userId = $userId;

    }

    public function account() {
        
        return $this->account;
    }

    protected function onJsonSerialize(array &$state) {
        $state["account"] = $this->account;
    }

    protected function userId() {
        return $this->userId;
    }

    protected function accountId() {
        return $this->account->id();
    }


}